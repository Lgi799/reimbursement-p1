
			
            function addnewreimbursement(){
                // The first step is to decide where you want to add a new element. In our case, we want to add a new reimbursement_div to the element which has the id "reimbursement_container". That means that we want to isolate the reimbursement_container div.

            let reimbursement_container = document.getElementById('reimbursement_container');

            // In order to add a new reimbursement_div to the DOM, we have to create it. Fortunately, there is a JS function which allows us to create a new element. You just pass in the name of the element that you'd like to create:

            let new_div = document.createElement('div');
            new_div.className = "reimbursement_div";

            // Unfortunately, you are going to have to set the content for h3, p and of course the needed attributes for elements that require them (e.g. img have "src" attributes).

            // Since the innerText and src should be dynamically provided by the user, we are going to refactor this code to pull that information from the input boxes the users put this information inside of. In order to do that, we have to isolate/grab the input boxes in order to grab the data that has been entered.

			let new_h32 = document.createElement('h3');
			new_h32.innerText = reimbursements.reimbursementid;
			
            let new_h3 = document.createElement('h3');
            new_h3.innerText = "Employee ID: " + reimbursement.employeeid;

            let new_para = document.createElement('p');
			new_para.innerText = "Amount: $" + reimbursement.reimbursementamount;

            let new_para2 = document.createElement('p');
            new_para2.innerText = "Description: " + reimbursement.reimbursementdescription;
            
            let new_para3 = document.createElement('p');
            new_para3.innerText = "Status: " + reimbursement.reimbursement_status;

            // We should also specify that the h3, p, img, and ol are children of the new div we're creating:

            new_div.appendChild(new_h32);
            new_div.appendChild(new_h3);
            new_div.appendChild(new_para);
            new_div.appendChild(new_para2);
            new_div.appendChild(new_para3);

            // Now that we've created that div and all of its child elements, let's FINALLY append the new div we've created to the existing reimbursement_container on our web page.

            reimbursement_container.appendChild(new_div);
            }
            let submit_button = document.querySelector('[type=submit]');

            // Now that I've isolated the input element, I want to add an event listener in order to prevent the default behavior that occurs when the button is clicked:
            
            submit_button.addEventListener('click', (event) => {
                //Technically speaking, we should check that the event is cancelable before we prevent the default behavior:
                if(event.cancelable){
                    event.preventDefault();
                }
            
                // Now let's add our own behavior to the button click. In other words, what should happen when the button is clicked?
                addnewreimbursement();
            
        })
        
    
    
  

