function getallreimbursement(){
    let url = 'http://localhost:8000/reimbursement/all';
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        // If the readyState is 4 and the HTTP status code is 200, I have access to the data I requested when I sent the HTTP request.
        if(xhr.readyState === 4 && xhr.status === 200){
            // My first order of business is to access the data itself. Note that the data comes to us as JSON but that we want to be able to use the data as a JavaScript object.
            let reimbursement= JSON.parse(xhr.response);

            for(let reimbursement of reimbursement){
                let reimbursement_container = document.getElementById('reimbursement_container');

                let new_div = document.createElement('div');
            new_div.className = "reimbursement_div";

			let new_h32 = document.createElement('h3');
			new_h32.innerText = reimbursements.reimbursementid;
			
            let new_h3 = document.createElement('h3');
            new_h3.innerText = "Employee ID: " + reimbursement.employeeid;

            let new_para = document.createElement('p');
			new_para.innerText = "Amount: $" + reimbursement.reimbursementamount;

            let new_para2 = document.createElement('p');
            new_para2.innerText = "Description: " + reimbursement.reimbursementdescription;
            
            let new_para3 = document.createElement('p');
            new_para3.innerText = "Status: " + reimbursement.reimbursement_status;

            new_div.appendChild(new_h32);
            new_div.appendChild(new_h3);
            new_div.appendChild(new_para);
            new_div.appendChild(new_para2);
            new_div.appendChild(new_para3);

            // Now that we've created that div and all of its child elements, let's FINALLY append the new div we've created to the existing reimbursement_container on our web page.

            reimbursement_container.appendChild(new_div);
            }
        }
    }
    xhr.open('GET', url); //readyState is 1

    // Now let's send our HTTP request.
    xhr.send(); //readyState is 2
}

// Any function you call here is going to be invoked as soon as the window loads.
window.onload = function(){
    this.getallreimbursement();
}





