package dao;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transaction;

import org.hibernate.Session;
import org.hibernate.HibernateException;

import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;
import model.Employee;
import model.Reimbursement;
import service.ReimbursementService;
import util.HibernateSessionFactory;

/*take find all method and make 2 versions findAll and mockAll(what we currentyl have
 * 
 * */

public class ReimbursementDAO{
	public List <Reimbursement> findAll() {
		/*List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee("un", "pw", "role"));
		employees.add(new Employee("nu", "wp", "role"));
		//that's the mockAll ^^
		
		return employees; 
		*/
		
		List<Reimbursement> reimbursements = null;
		Session s = null;
		org.hibernate.Transaction tx = null;
		//Employee pass = null;
		
		try {
			s = HibernateSessionFactory.getSession(); //source of error, check config file
			tx = s.beginTransaction();
			reimbursements = s.createQuery("FROM Reimbursment", Reimbursement.class).getResultList();
			tx.commit();

		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}finally {
			s.close();
		}
		
		return reimbursements;

	
	}
	
	public Reimbursement save(Reimbursement reimbursement) { //Transient State
		/*
		 * Your session allows you to perform work on your DB. All work
		 * (e.g. basic CRUD) is done within the context of a Hibernate
		 * session.
		 */
		Session session = null;
		/*
		 * The transaction interface allows you finalize and/or revert
		 * changes to your DB.
		 */
		org.hibernate.Transaction tx = null;
		
		try {
			session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			/*
			 * The "save" method persists a new record to your DB.
			 */
			session.save(reimbursement); //Persistent State
			tx.commit();
		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}finally {
			session.close(); //Detached State
		}
		return reimbursement;

	}

	public Reimbursement findByID(int reimbursementid) {
			Reimbursement retrievedReimbursement= null;
		
		Session s = null;
		org.hibernate.Transaction tx = null;
		
		try {
			s = HibernateSessionFactory.getSession();
			tx = s.beginTransaction();
			/*
			 * CriteriaBuilder allow us to construct CriteriaQuery.
			 */
			CriteriaBuilder cb = s.getCriteriaBuilder();
			
			/*
			 * Our CriteriaQuery is an object representation of a query that has criteria imposed on it (e.g.
			 * where clauses, having, groupby, etc...).
			 */
			CriteriaQuery<Reimbursement> cq = cb.createQuery(Reimbursement.class);
			
			//select * from ROOT
			Root<Reimbursement> root = cq.from(Reimbursement.class);
			
			// What are the criteria we're building out? For us, it's a "where clause".
			cq.select(root).where(cb.equal(root.get("reimbursementid"), reimbursementid));
			
			// Finally, take your CriteriaQuery and create a standard query using the Query interface.
			Query<Reimbursement> query = s.createQuery(cq);
			
			
			/*
			 * NOTE: Make sure that your DB constraints allow for you to call such a method as getSingleResult as
			 * this method assumes that you really will get a single result. Also note that getSingleResult throws
			 * an exception if your record doesn't exist.
			 * 
			 * uniqueResult(), on the other hand, returns null if no such record exists.
			 */
			retrievedReimbursement = query.uniqueResult();
			
			tx.commit();
		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}
		finally {
			s.close();
		}
		
		return retrievedReimbursement;
	}


		
	}
