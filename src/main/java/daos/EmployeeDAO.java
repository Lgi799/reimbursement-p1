package dao;

import javax.transaction.Transaction;

import org.hibernate.Session;
import org.hibernate.HibernateException;
import java.util.List;
import model.Employee;
import util.HibernateSessionFactory;

public class EmployeeDAO{
	public List <Employee> findAll() {
		List <Employee> employee = null;
		Session s = null;
		org.hibernate.Transaction tx = null;
		Employee pass = null;
		
		try {
			s = HibernateSessionFactory.getSession();
			tx = s.beginTransaction();
			employee = s.createQuery("FROM employee", Employee.class).getResultList();
			tx.commit();

		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}finally {
			s.close();
		}
		
		return employee;

	
	}
	
	public void save(Employee employee) { //Transient State
		/*
		 * Your session allows you to perform work on your DB. All work
		 * (e.g. basic CRUD) is done within the context of a Hibernate
		 * session.
		 */
		Session session = null;
		/*
		 * The transaction interface allows you finalize and/or revert
		 * changes to your DB.
		 */
		org.hibernate.Transaction tx = null;
		
		try {
			session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			/*
			 * The "save" method persists a new record to your DB.
			 */
			session.save(employee); //Persistent State
			tx.commit();
		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}finally {
			session.close(); //Detached State
		}
	}
	

}
