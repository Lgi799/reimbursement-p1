package service;

import java.util.List;

import model.Employee;
import dao.EmployeeDAO;

// This is the service layer of my application. It is reserved for business logic (e.g.
// filtering data, transforming data.
public class EmployeeService {
	
	private EmployeeDAO employeedao;
	
	public EmployeeService() {
		this.employeedao = new EmployeeDAO();
	}

	// If we have a service layer, we can avoid repetitive code in our repository/DAO layer.
	// We can simply pass our data from the repository to the service layer in order to
	// perform our business logic here.
	
	
	public List<Employee> findAll(){
		return this.employeedao.findAll();
	}
	
	public void save(Employee employee) {
		this.employeedao.save(employee);
	}
	
	//public Recipe findByName(String name) {
		//return this.recipeRepository.findByName(name);
	}

