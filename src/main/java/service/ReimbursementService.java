package service;

import java.util.List;

import dao.EmployeeDAO;
import dao.ReimbursementDAO;
import model.Employee;
import model.Reimbursement;

public class ReimbursementService {
	
	private ReimbursementDAO reimbursementdao;
	
	public ReimbursementService(ReimbursementDAO reimbDAO) {
		this.reimbursementdao = reimbDAO;
	}

	// If we have a service layer, we can avoid repetitive code in our repository/DAO layer.
	// We can simply pass our data from the repository to the service layer in order to
	// perform our business logic here.
	
	
	public List<Reimbursement> findAll(){
		return this.reimbursementdao.findAll();
		
		
	}
	
	public Reimbursement save(Reimbursement reimbursement) {
		return this.reimbursementdao.save(reimbursement);
	}
	
	public Reimbursement findByID(int employeeid) {
		return this.reimbursementdao.findByID(employeeid);
	}

}
