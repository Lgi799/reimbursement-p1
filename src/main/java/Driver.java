import javax.servlet.http.HttpSession;

import controller.EmployeeController;
import controller.TestController;
import dao.EmployeeDAO;
import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import service.EmployeeService;
import service.ReimbursementService;
import dao.ReimbursementDAO;
import controller.ReimbursementController;
public class Driver {
public static void main(String...main) {
		
		//I'm starting my server on port 8000.
		Javalin app = Javalin.create().start(8000);
		
		app.post("/login", ctx -> {
			//If user credentials are correct, grant an HttpSession:
			ctx.req.getSession();
		});
		app.get("/logout", ctx -> {
			//If you pass in "false", an existing session is checked for.
			HttpSession session = ctx.req.getSession(false);
			if(session != null) session.invalidate();
		});
		
		app.after(ctx -> {
			ctx.res.addHeader("Access-Control-Allow-Origin", "null");
		});
			
		app.config.addStaticFiles("/web", Location.CLASSPATH);
		TestController testcontroller = new TestController(app);
		EmployeeDAO employeeDAO = new EmployeeDAO(); //could make mock or hibernate employee DAO
		EmployeeService employeeservice = new EmployeeService(employeeDAO);
		EmployeeController employeeController = new EmployeeController(app, employeeservice);
		ReimbursementDAO reimbursementDAO = new ReimbursementDAO(); //could make mock or hibernate employee DAO
		ReimbursementService reimbursementservice = new ReimbursementService(reimbursementDAO);
		ReimbursementController reimbursmentController = new ReimbursementController(app, reimbursementservice);
	
}
}

