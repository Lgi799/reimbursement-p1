package controller;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;


import model.Employee;


import service.EmployeeService;

import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.Handler;

public class EmployeeController{
	
	private EmployeeService employeeservice;
	public Employee emp;
	
	public EmployeeController (Javalin app, EmployeeService empSvc) {
		this.employeeservice = empSvc;
		app.routes(() -> {
			path("/employee", () -> {
				//We can do subroutes for reimbursement resources (e.g. /reimbursement/all or /reimbursement/:id
				path("/:id", () -> {
					get(login);
				});
				path("/login", () -> {
					post(login2);
				});
				
			});
		});
	}
		
		
	
	
	
	/*private Handler findAll = ctx -> {
		HttpSession session = ctx.req.getSession(false);
		
		if (session != null)
			ctx.json(this.employeeservice.findAll());
		else
			ctx.res.getWriter().write("{\"error\": \"You do not have a session.\"}");
	};
	*/
	
	private Handler login = ctx -> {
		
		//To check for the existence of a session:
		HttpSession session = ctx.req.getSession(false);
		
		emp = this.employeeservice.findByID(Integer.parseInt(ctx.pathParam("employeeid")));
		ctx.json(emp);
	};
	
private Handler login2 = ctx -> {
		
		//To check for the existence of a session:
		HttpSession session = ctx.req.getSession(false);
		
		emp = this.employeeservice.findByID(Integer.parseInt(ctx.pathParam("employeeid")));
		
		if (emp.getPassword().equals(ctx.req.getParameter("password")) && emp.getUserrole().equals("Employee")) {
			ctx.redirect("/home.html"); //change to redirect to employee dashboard
		}
		else if (emp.getPassword().equals(ctx.req.getParameter("password")) && emp.getUserrole().equals("Manager")) {
			ctx.redirect("/managerportal.html"); //change to redirect to manager dashboard
		}
		else {
			ctx.redirect("/reimbursementportal.html");
		}
	};


	/*public  void getAll(Context ctx) {
		 List<Employee> rows = this.employeeservice.findAll();
		  
	        //Customer row = dao.get(Integer.parseInt(ctx.pathParam("id")));
	        ctx.json(rows);
	    }
	    */
		
	}


