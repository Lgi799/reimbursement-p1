package controller;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import model.Employee;
import model.Reimbursement;


import service.ReimbursementService;

import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.Handler;

public class ReimbursementController {

		private ReimbursementService reimbursementservice;
		
		public ReimbursementController(Javalin app, ReimbursementService reimSVC) {
			this.reimbursementservice = reimSVC;
			app.routes(() -> {
				path("/reimbursement", () -> {
					
					path("/all", () -> {
						
						get(findAll);
					});
					path("/new", () -> {
						post(save);
					});
					path("/:employeeid", () -> {
						get(findByID);
					});
					
				});
			});
		}
		private Handler findAll = ctx -> {
			
		
			HttpSession session = ctx.req.getSession(false);
			
			if(session != null || true) //potential issue with null
				ctx.json(this.reimbursementservice.findAll());
			else
				ctx.res.getWriter().write("{\"error\": \"You do not have a session.\"}");
			
			};
			private Handler save = ctx -> {
				
				/*
				 * You can access input from a form by using the getParameter method
				 * that the context "req" property gives you access to.
				 */
				Reimbursement reimbursement = new Reimbursement(1, 
						Double.parseDouble(ctx.req.getParameter("reimbursementamount")),
						ctx.req.getParameter("reimbursementdescription"),
						Integer.parseInt(ctx.req.getParameter("employeeid")),
						ctx.req.getParameter("reimbursement_status")
						);
				
				this.reimbursementservice.save(reimbursement);
				this.reimbursementservice.findAll();
				
				/*
				 * Redirecting to a different view is simple in Javalin. You just
				 * use the "redirect" method that the context gives you access to.
				 */
				ctx.redirect("/employeeportal.html");//change to like employee dashboard
				
			};
			
			private Handler findByID = ctx -> {
				Reimbursement retrievedreimbursement = this.reimbursementservice.findByID(Integer.parseInt(ctx.pathParam("employeeid")));
				ctx.json(retrievedreimbursement);
			};
			



}



