package util;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactory{
	//follow singleton design pattern
	
	private static SessionFactory sessionFactory;
	
	public static Session getSession() {
		if(sessionFactory == null) {

	
	

	try {
		Properties props = new Properties();
		FileReader fileread = new FileReader("src/main/resources/connection.properties");
		props.load(fileread);
		//may need to concatenate the url string to include the port?
		
		Configuration config = new Configuration();
		config.configure();
		//config.configure("src/main/resources/hibernate.cfg.xml");
				config.setProperty("hibernate.connection.username", props.getProperty("username"));
				config.setProperty("hibernate.connection.password", props.getProperty("password"));
				config.setProperty("hibernate.connection.url", props.getProperty("url"));
				
		sessionFactory = config.buildSessionFactory();

	}catch (IOException e) {
		e.printStackTrace();

		}
	
	return sessionFactory.getCurrentSession();
} else {
	return sessionFactory.getCurrentSession();
}

}
}
	
