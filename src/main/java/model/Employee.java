package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



//Entity makes this class as an entity for us, meaning that we intend to map this class to a table in our DB.
@Entity

//The Table annotations allow us to specify information about the table we want our associated with our model.
//You can, for instance, specify the table's name (the DB table). That said, you don't have to specify the table
//name as Hibernate will just use the model's name as the table name if you specify no name.
@Table(name = "employee")
public class Employee {
	

	/*
	 * The Id annotation denotes that we wish to use this field as a primary key on
	 * this table.
	 */
	@Id //represents primary key

	@Column(name ="username") //do the same for each column in the table
	private String username;
	@Column(name ="password")
	private String password;
	@Column(name ="manager")
	private int manager;
	
	
	public Employee() {

	}

	public Employee (String username, String password, int manager) {
		setUsername(username);
		setPassword(password);
		setManager(manager);
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public int setManger() {
		return manager;
	}
	
	public void setManager(int manager) {
		this.manager = manager;
	}



}
