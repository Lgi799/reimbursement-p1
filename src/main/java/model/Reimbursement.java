package model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity

@Table(name = "reimbursement")

public class Reimbursement {
	@Id
	/*
	 * The Column annotation allows us to specify that this field should be a column on my table.
	 */
	@Column(name="reimbursementid")
	//@Column(name="reimbursementid")
	/*
	 * The GeneratedValue annotation specifies that the column's value is auto-generated (in other words, you're using
	 * some sort of sequence or auto-incrementing data type).
	 */
	//@GeneratedValue(generator = "reimbursement_id_seq", strategy = GenerationType.AUTO)
	/*
	 * We also have to generate the sequence here
	 */
	//@SequenceGenerator(allocationSize = 1, name = "reimbursement_id_seq", sequenceName = "reimbursement_id_seq")
	private int reimbursementid;
	@Column(name ="reimbursementamount")
	private double reimbursementamount;
	@Column(name = "reimbursementdescription")
	private String reimbursementdescription;
	@Column(name="employeeid")
	private int employeeid;
	@Column(name = "reimbursement_status")
	private String reimbursement_status;
	

	public Reimbursement() {
		super();
	}
	public Reimbursement(int reimbursementid, double reimbursementamount, String reimbursementdescription,  int employeeid,  String reimbursement_status) {
		super();
		this.reimbursementid = reimbursementid;
		this.reimbursementamount = reimbursementamount;
		this.reimbursementdescription = reimbursementdescription;
		this.employeeid = employeeid;
		this.reimbursement_status = reimbursement_status;
	}


	public int getReimbursementid() {
		return reimbursementid;
	}

	public void setReimbursementid(int reimbursementid) {
		this.reimbursementid = reimbursementid;
	}

	public double getReimbursementamount() {
		return reimbursementamount;
	}

	public void setReimbursementamount(double reimbursementamount) {
		this.reimbursementamount = reimbursementamount;
	}

	public String getReimbursementdescription() {
		return reimbursementdescription;
	}

	public void setReimbursementdescription(String reimbursementdescription) {
		this.reimbursementdescription = reimbursementdescription;
	}

	
	public int getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(int employeeid) {
		this.employeeid = employeeid;
	}
	
	public String getReimbursement_status() {
		return reimbursement_status;
	}

	public void setReimbursement_status(String reimbursement_status) {
		this.reimbursement_status = reimbursement_status;
	}





}
