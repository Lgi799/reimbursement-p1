DROP DATABASE IF EXISTS reimb;
CREATE DATABASE reimb;

USE reimb;
DROP TABLE IF EXISTS employee;
CREATE TABLE employee(
	username VARCHAR(40) NOT NULL,
	employeeid INT AUTO_INCREMENT, 
	password VARCHAR(40) DEFAULT 'pass123',
	userrole VARCHAR (40) NOT NULL,
	CONSTRAINT pk_employeeid PRIMARY KEY (employeeid) #userID column should be pk
	#id INT NOT NULL auto_increment,#
	#will need get id and set id in employee model#
);



#dummy row to test#
INSERT INTO employee (username, password, userrole) VALUES ("jdoe",  'pass123', 'employee');
INSERT INTO employee (username, password,userrole) VALUES ("msmith",  'pass123', 'manager');
SELECT * FROM employee;

DROP TABLE IF EXISTS reimbursement;
CREATE TABLE reimbursement(
	reimbursementid INT AUTO_INCREMENT,
	reimbursementamount DOUBLE ,
	reimbursementdescription VARCHAR (80) NOT NULL,
	employeeid INT ,
	reimbursement_status VARCHAR (80) NOT NULL,
	CONSTRAINT pk_reimbursementid PRIMARY KEY (reimbursementid)
	#CONSTRAINT fk_employeeid FOREIGN KEY (employeeid)
	#REFERENCES employee(employeeid)

);

INSERT INTO reimbursement ( reimbursementamount, reimbursementdescription, employeeid, reimbursement_status) VALUES ( 80, 'plane', 1, 'pending');

SELECT * FROM reimbursement;

DROP TABLE IF EXISTS reimbursement2;
CREATE TABLE reimbursement2(
	reimbursementid INT AUTO_INCREMENT,
	
	reimbursementdescription VARCHAR (80) NOT NULL,
	
	CONSTRAINT pk_reimbursementid2 PRIMARY KEY (reimbursementid)
	#CONSTRAINT fk_employeeid FOREIGN KEY (employeeid)
	#REFERENCES employee(employeeid)

);
