package com.revature.controller;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpSession;

import com.revature.model.Author;
import com.revature.model.Ingredient;
import com.revature.model.Recipe;
import com.revature.service.RecipeService;

import io.javalin.Javalin;
import io.javalin.http.Handler;

public class RecipeController {

	private RecipeService recipeService;
	
	public RecipeController(Javalin app) {
		this.recipeService = new RecipeService();
		app.routes(() -> {
			path("/recipe", () -> {
				//We can do subroutes for recipe resources (e.g. /recipe/all or /recipe/:id
				path("/all", () -> {
					//This is GET handler for the /recipe/all endpoint.
					get(findAllRecipes);
				});
				path("/new", () -> {
					post(saveRecipe);
				});
				path("/name/:name", () -> {
					//http://localhost:8000/recipe/name/whateverrecipename
					get(recipeByName);
				});
			});
		});
	}
	
	/*
	 * Why are we using so many lambda expressions? Javalin makes heavy use of lambda expressions.
	 * The Handler interface is a function interface that we can use to create lambdra expressions
	 * which take in a context that gives us access to our HTTP request and response bodies.
	 */
	
	// This lambda expression writes the list of recipes to the response body as JSON.
	
	private Handler findAllRecipes = ctx -> {
		
		//To check for the existence of a session:
		HttpSession session = ctx.req.getSession(false);
		
		if(session != null)
			ctx.json(this.recipeService.findAll());
		else
			ctx.res.getWriter().write("{\"error\": \"You do not have a session.\"}");
		
		};
		
	private Handler saveRecipe = ctx -> {
		
		Set<Ingredient> affiliatedIngredients = new HashSet<>();
		affiliatedIngredients.add(new Ingredient(1, "n/a"));
		affiliatedIngredients.add(new Ingredient(3, "n/a"));
		
		/*
		 * You can access input from a form by using the getParameter method
		 * that the context "req" property gives you access to.
		 */
		Recipe recipe = new Recipe(1, 
				ctx.req.getParameter("recipe_name"),
				ctx.req.getParameter("recipe_description"),
				ctx.req.getParameter("image_url"),
				ctx.req.getParameter("recipe_step"),
				new Author(1, "N/A"), affiliatedIngredients);
		
		this.recipeService.save(recipe);
		
		/*
		 * Redirecting to a different view is simple in Javalin. You just
		 * use the "redirect" method that the context gives you access to.
		 */
		ctx.redirect("/home.html");
		
	};
	
	private Handler recipeByName = ctx -> {
		Recipe retrievedRecipe = this.recipeService.findByName(ctx.pathParam("name"));
		ctx.json(retrievedRecipe);
	};
	
}
