package com.revature.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

/*
 * Remember that Hibernate is designed to help us fix the paradigm mismatch between SQL and Java, which
 * is an OOP language. As such, we don't have to write any SQL in order to persist records. We can instead
 * let Hibernate take care of generating the queries. That said, in order for this to happen, we have to provide
 * some "mappings" from Hibernate so that the ORM frameworks knows how a Java model correlates to a table in
 * our DB. In fact, we have to tell Hibernate whether or not a model should even represent a table in our DB.
 * 
 * We can do this via several annotations.
 */

// Entity makes this class as an entity for us, meaning that we intend to map this class to a table in our DB.
@Entity

// The Table annotations allow us to specify information about the table we want our associated with our model.
// You can, for instance, specify the table's name (the DB table). That said, you don't have to specify the table
// name as Hibernate will just use the model's name as the table name if you specify no name.
@Table(name = "hibernate_recipe")
public class Recipe {

	/*
	 * The Id annotation denotes that we wish to use this field as a primary key on
	 * this table.
	 */
	@Id
	/*
	 * The Column annotation allows us to specify that this field should be a column
	 * on my table.
	 */
	@Column(name = "recipe_id")
	/*
	 * The GeneratedValue annotation specifies that the column's value is
	 * auto-generated (in other words, you're using some sort of sequence or
	 * auto-incrementing data type).
	 */
	@GeneratedValue(generator = "recipe_id_seq", strategy = GenerationType.AUTO)
	/*
	 * We also have to generate the sequence here
	 */
	@SequenceGenerator(allocationSize = 1, name = "recipe_id_seq", sequenceName = "recipe_id_seq")
	private int recipe_id;
	@Column
	private String recipe_name;
	@Column
	private String recipe_description;
	@Column
	private String image_url;
	@Column
	private String recipe_step;

	/*
	 * If you wish to create a foreign key, you need only specify the type directly
	 * here and use the ManyToOne annotation.
	 * 
	 * When you have single nested associations like this, Hibernate will eagerly
	 * fetch the associated record. This is officially called "eager fetching".
	 * 
	 * Note that the alternative to eager fetching is "lazy loading". Please note
	 * that lazy loading is the default with collections whereas eager fetching is
	 * the default for single associations.
	 * 
	 * Also note that if you are using lazy loading, Hibernate will use "proxies"
	 * for the objects that have not been loaded yet. These proxies are really just
	 * stand-ins for the objects that have been yet initialized yet. Hibernate
	 * creates proxies by subclassing your models and using those subclasses as the
	 * proxies until necessary data is requested (e.g. a getter is called on the
	 * proxy). As a fun fact, this is why your Hibernate entities/models should not
	 * be final.
	 */
	@ManyToOne
	private Author author;

	// Specifying a many-to-many relationship between recipe and ingredient
	@ManyToMany(fetch = FetchType.EAGER)

	/*
	 * We also want Hibernate to create a bridge table between these two entities.
	 * In order do this, we'll use
	 * 
	 * @JoinTable annotation.
	 */

	@JoinTable(joinColumns = { @JoinColumn(name = "recipe_id") }, inverseJoinColumns = {
			@JoinColumn(name = "ingredient_id") })
	private Set<Ingredient> ingredients;

	public Recipe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Recipe(int recipe_id, String recipe_name, String recipe_description, String image_url, String recipe_step,
			Author author, Set<Ingredient> ingredients) {
		super();
		this.recipe_id = recipe_id;
		this.recipe_name = recipe_name;
		this.recipe_description = recipe_description;
		this.image_url = image_url;
		this.recipe_step = recipe_step;
		this.author = author;
		this.ingredients = ingredients;
	}

	public int getRecipe_id() {
		return recipe_id;
	}

	public void setRecipe_id(int recipe_id) {
		this.recipe_id = recipe_id;
	}

	public String getRecipe_name() {
		return recipe_name;
	}

	public void setRecipe_name(String recipe_name) {
		this.recipe_name = recipe_name;
	}

	public String getRecipe_description() {
		return recipe_description;
	}

	public void setRecipe_description(String recipe_description) {
		this.recipe_description = recipe_description;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getRecipe_step() {
		return recipe_step;
	}

	public void setRecipe_step(String recipe_step) {
		this.recipe_step = recipe_step;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Set<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Set<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((image_url == null) ? 0 : image_url.hashCode());
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((recipe_description == null) ? 0 : recipe_description.hashCode());
		result = prime * result + recipe_id;
		result = prime * result + ((recipe_name == null) ? 0 : recipe_name.hashCode());
		result = prime * result + ((recipe_step == null) ? 0 : recipe_step.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Recipe other = (Recipe) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (image_url == null) {
			if (other.image_url != null)
				return false;
		} else if (!image_url.equals(other.image_url))
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (recipe_description == null) {
			if (other.recipe_description != null)
				return false;
		} else if (!recipe_description.equals(other.recipe_description))
			return false;
		if (recipe_id != other.recipe_id)
			return false;
		if (recipe_name == null) {
			if (other.recipe_name != null)
				return false;
		} else if (!recipe_name.equals(other.recipe_name))
			return false;
		if (recipe_step == null) {
			if (other.recipe_step != null)
				return false;
		} else if (!recipe_step.equals(other.recipe_step))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Recipe [recipe_id=" + recipe_id + ", recipe_name=" + recipe_name + ", recipe_description="
				+ recipe_description + ", image_url=" + image_url + ", recipe_step=" + recipe_step + ", author="
				+ author + ", ingredients=" + ingredients + "]";
	}

}
