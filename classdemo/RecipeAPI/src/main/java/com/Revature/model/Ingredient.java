package com.revature.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity

@Table(name="hibernate_ingredient")
public class Ingredient {

	@Id
	@Column
	@GeneratedValue(generator = "ingredient_id_seq", strategy = GenerationType.AUTO)
	@SequenceGenerator(allocationSize = 1, name = "ingredient_id_seq", sequenceName = "ingredient_id_seq")
	private int ingredient_id;
	@Column
	private String ingredient_name;

	public Ingredient() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Ingredient(int ingredient_id, String ingredient_name) {
		super();
		this.ingredient_id = ingredient_id;
		this.ingredient_name = ingredient_name;
	}

	public int getIngredient_id() {
		return ingredient_id;
	}

	public void setIngredient_id(int ingredient_id) {
		this.ingredient_id = ingredient_id;
	}

	public String getIngredient_name() {
		return ingredient_name;
	}

	public void setIngredient_name(String ingredient_name) {
		this.ingredient_name = ingredient_name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ingredient_id;
		result = prime * result + ((ingredient_name == null) ? 0 : ingredient_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingredient other = (Ingredient) obj;
		if (ingredient_id != other.ingredient_id)
			return false;
		if (ingredient_name == null) {
			if (other.ingredient_name != null)
				return false;
		} else if (!ingredient_name.equals(other.ingredient_name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Ingredient [ingredient_id=" + ingredient_id + ", ingredient_name=" + ingredient_name + "]";
	}

}
