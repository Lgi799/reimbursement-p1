package com.revature.service;

import java.util.List;

import com.revature.model.Recipe;
import com.revature.repository.RecipeRepository;

// This is the service layer of my application. It is reserved for business logic (e.g.
// filtering data, transforming data.
public class RecipeService {
	
	private RecipeRepository recipeRepository;
	
	public RecipeService() {
		this.recipeRepository = new RecipeRepository();
	}

	// If we have a service layer, we can avoid repetitive code in our repository/DAO layer.
	// We can simply pass our data from the repository to the service layer in order to
	// perform our business logic here.
	public List<Recipe> findAllRecipesAlphabetical(){
		List<Recipe> allRecipes = this.recipeRepository.findAll();
		
		//Now do custom sorting/transformation of the data. Note: I didn't actually sort 
		//anything here.
		return null;
	}
	
	public List<Recipe> findAll(){
		return this.recipeRepository.findAll();
	}
	
	public void save(Recipe recipe) {
		this.recipeRepository.save(recipe);
	}
	
	public Recipe findByName(String name) {
		return this.recipeRepository.findByName(name);
	}
}
