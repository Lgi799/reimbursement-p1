package com.revature.repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.revature.model.Recipe;
import com.revature.util.HibernateSessionFactory;

//This is my DAO layer. It has been rewritten with Hibernate.
public class RecipeRepository {

	public List<Recipe> findAll(){
		
		List<Recipe> recipes = null;
		
		// We need a session to do work on our DB
		Session s = null;
		Transaction tx = null;
		
		try {
			s = HibernateSessionFactory.getSession();
			tx = s.beginTransaction();
			/*
			 * Hibernate has its own query language called "HQL" - Hibernate Query Language.
			 * HQL allows us to emphasize our Java models rather than the entities in our DB when we
			 * are making queries. It provides a more object-oriented approach to data persistence.
			 * 
			 * I should point out that you have the option to use native SQL (i.e. plain old SQL). You can simply
			 * do so by calling "createNativeQuery".
			 */
			recipes = s.createQuery("FROM Recipe", Recipe.class).getResultList();
			tx.commit();
		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}finally {
			s.close();
		}
		
		
		return recipes;
	}
	
	
	
	/*
	 * This is a standard Hibernate workflow. It uses common interfaces
	 * of Hibernate in order to persist a new record.
	 * 
	 * As an additional note, objects have different "states" in Hibernate.
	 * There are 3 different object states in Hibernate:
	 * 
	 * Transient: An object that has not yet been associated with a 
	 * session and has no representation in your DB
	 * Persistent: An object that has been associated with a Hibernate
	 * session and has a representation in your DB.
	 * Detached: An object becomes detached after it is no longer associated
	 * with a session.
	 */
	public void save(Recipe recipe) { //Transient State
		/*
		 * Your session allows you to perform work on your DB. All work
		 * (e.g. basic CRUD) is done within the context of a Hibernate
		 * session.
		 */
		Session session = null;
		/*
		 * The transaction interface allows you finalize and/or revert
		 * changes to your DB.
		 */
		Transaction tx = null;
		
		try {
			session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			/*
			 * The "save" method persists a new record to your DB.
			 */
			session.save(recipe); //Persistent State
			tx.commit();
		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}finally {
			session.close(); //Detached State
		}
	}
	
	public Recipe findByName(String name) {
		
		Recipe retrievedRecipe = null;
		
		Session s = null;
		Transaction tx = null;
		
		try {
			s = HibernateSessionFactory.getSession();
			tx = s.beginTransaction();
			/*
			 * CriteriaBuilder allow us to construct CriteriaQuery.
			 */
			CriteriaBuilder cb = s.getCriteriaBuilder();
			
			/*
			 * Our CriteriaQuery is an object representation of a query that has criteria imposed on it (e.g.
			 * where clauses, having, groupby, etc...).
			 */
			CriteriaQuery<Recipe> cq = cb.createQuery(Recipe.class);
			
			//select * from ROOT
			Root<Recipe> root = cq.from(Recipe.class);
			
			// What are the criteria we're building out? For us, it's a "where clause".
			cq.select(root).where(cb.equal(root.get("recipe_name"), name));
			
			// Finally, take your CriteriaQuery and create a standard query using the Query interface.
			Query<Recipe> query = s.createQuery(cq);
			
			
			/*
			 * NOTE: Make sure that your DB constraints allow for you to call such a method as getSingleResult as
			 * this method assumes that you really will get a single result. Also note that getSingleResult throws
			 * an exception if your record doesn't exist.
			 * 
			 * uniqueResult(), on the other hand, returns null if no such record exists.
			 */
			retrievedRecipe = query.uniqueResult();
			
			tx.commit();
		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}
		finally {
			s.close();
		}
		
		return retrievedRecipe;
	}
}
